/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __APPUART_H
#define __APPUART_H
#ifdef __cplusplus
 extern "C" {
#endif
/* Includes -------------------------------------------------------------------*/

#include "interface.h"
#include "main.h"
#include "timer.h"
#include "Queue.h"

/* typedef --------------------------------------------------------------------*/
//#define FC_01_READ_COIL						0x01
//#define FC_02_READ_INPUT_STATUS			0x02
//#define FC_03_READ_HOLDING_REGISTERS	0x03
#define FC_04_READ_INPUT_REGISTERS		0x04
//#define FC_05_FORCE_SINGLE_COIL			0x05
#define FC_06_PRESET_SINGLE_REGISTER	0x06
//#define FC_15_FORCE_MULTIPLE_COILS		0x0F
#define FC_16_PRESET_MULTIPLE_REGISTERS	0x10
	 
	 
#define MODBUS_SLAVE_ID		0xAA
#define MODBUS_POS_SA		0
#define MODBUS_POS_FC		1
#define MODBUS_POS_CMD		7

	 
//#define ADDRESS_CHECK_COMM		1000
//#define ADDRESS_COMMAND			1000
//#define ADDRESS_ROBOT_TYPE		1001
//	 
//#define ADDRESS_MOTION			1010
//#define ADDRESS_DELAYS			1030
//	 
//#define ADDRESS_COUNT			1050 
//#define ADDRESS_SETTING			1070
//#define ADDRESS_FUNC				1100
//
//#define ADDRESS_IO				1110
//#define ADDRESS_SEQ				1150
	 
#define ADDRESS_CHECK_COMM		1000	//1000
#define ADDRESS_COMMAND			1000
	 
#define ADDRESS_IO				1002
#define ADDRESS_SEQ				1033		//COUNT+SEQ
#define ADDRESS_MANUAL			1000
#define ADDRESS_STEP				1000
#define ADDRESS_CYCLE			1000
#define ADDRESS_AUTO				1000
	 
	 
#define ERROR_STX			0x01
#define ERROR_SIZE		0x02
#define ERROR_NONE		0x00
#define ERROR_IDH			0x03
#define ERROR_OTHER		0x04
#define ERROR_CHKSUM		0x05
#define ERROR_ETX			0x06

#define TIME_COMM_READY 200

#define TIME_TEST_HW			1
#define TIME_COMM_SCAN		6		// 1/ 115200bps / 640bits =5.56ms

#define TIME_TIME_OUT	
#define TIME_DELAY_RCV		15		//Maximum rcv time for modbus packet
#define TIME_DELAY_SEND		7
#define TIME_RCV_TIMEOUT	100
#define CRC_ERROR_NONE		0x00
#define SA_NOT_MATCH			0x05
#define CRC_ERROR				0x01
#define CRC_ERROR_MISS		0x02
#define CRC_ERROR_HI			0x03
#define CRC_ERROR_LOW		0x04

#define SIZE_COUNT			sizeof(COUNT_MSG)			//((sizeof(COUNT_MSG)+1)/2)
#define SIZE_IO				sizeof(IO_MSG)		//((sizeof(IO_MSG)+1)/2)	//ODD
#define SIZE_OUTPUT			18
#define SIZE_INPUT			13
#define SIZE_SEQ				(SIZE_SEQARR+1)	//1 : SeqPos		//((sizeof(102)+1)/2)
	
#define SIZE_SETTING			sizeof(MANUFACTURER_SETTING)
	 
	 
	 
#define SIZE_SEND_SEQ		(uint16_t)(SIZE_SEQ + SIZE_COUNT)
#define SIZE_INIT_IO			(uint16_t)(SIZE_IO + 1)	//1 : Mode
					//SEQ
#define SIZE_MANUAL			(uint16_t)(SIZE_IO + 1)	//1 : Mode	// IO 		//Response : Add Err
#define SIZE_STEP				(uint16_t)(SIZE_IO + 2)//2 : Mode + SeqPos//IO + COUNT + STEP
#define SIZE_CYCLE			(uint16_t)(SIZE_IO + 2)//IO  + STEP
#define SIZE_AUTO				(uint16_t)(SIZE_IO + SIZE_COUNT + SIZE_COUNT + 2)//IO + COUNT + STEP

/* define ---------------------------------------------------------------------*/
typedef struct _MODBUS_MSG{
	uint8_t SlaveNo;
	uint8_t FuncCode;
	uint16_t Address;
	uint16_t Size;
	uint8_t ByteLen;
	uint8_t* data;
	uint8_t TimeOut;
}MODBUS_MSG;
/* macro ----------------------------------------------------------------------*/

/* function prototypes --------------------------------------------------------*/
//extern void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
//extern void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart);
extern void LoopAppUart(void);
extern void InitQueue(void);
/* variables ------------------------------------------------------------------*/

/* ----------------------------------------------------------------------------*/




#ifdef __cplusplus
}
#endif
#endif /*__APPUART_H */