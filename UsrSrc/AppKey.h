/* 
 * File:   AppKey.h
 * Author: Jay Park
 *
 * Created on 2016년 11월 14일 (Mon), PM 3:33
 */

#ifndef __APPTKEY_H
#define	__APPTKEY_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "interface.h"
#include "main.h"   
   

#define TIME_KEY 50
#define TIME_SCAN	0
	
#define ResetKey()		GPIOF->BSRR = (0x1F << 5)
#define RowKey(x)			GPIOF->BSRR = (0x01 << (x + 21))
#define ReadKey()			(((GPIOF->IDR) & 0x1F) ^ 0x1F)
	
#define MAX_ROW			5
	
#define KEY_BIT0		0x00000001		//0,0
#define KEY_BIT1		0x00000002     //0,1
#define KEY_BIT2		0x00000004     //0,2		//SHIFT
#define KEY_BIT3		0x00000008     //0,3
#define KEY_BIT4		0x00000010     //0,4
#define KEY_BIT5		0x00000020     //1,0
#define KEY_BIT6		0x00000040     //1,1
#define KEY_BIT7		0x00000080     //1,2
#define KEY_BIT8		0x00000100     //1,3
#define KEY_BIT9		0x00000200     //1,4
#define KEY_BIT10		0x00000400     //2,0
#define KEY_BIT11		0x00000800     //2,1
#define KEY_BIT12		0x00001000     //2,2
#define KEY_BIT13		0x00002000     //2,3
#define KEY_BIT14		0x00004000     //2,4
#define KEY_BIT15		0x00008000     //3,0
#define KEY_BIT16		0x00010000     //3,1
#define KEY_BIT17		0x00020000     //3,2
#define KEY_BIT18		0x00040000     //3,3
#define KEY_BIT19		0x00080000     //3,4
#define KEY_BIT20		0x00100000     //4,0
#define KEY_BIT21		0x00200000     //4,1
#define KEY_BIT22		0x00400000     //4,2
#define KEY_BIT23		0x00800000     //4,3
#define KEY_BIT24		0x01000000     //4,4

	
#define KEY_MENU_TIMER	    		KEY_BIT24
#define KEY_MENU_MODE	         KEY_BIT23
#define KEY_MENU_STEP	         KEY_BIT22
#define KEY_MENU_AUTO				KEY_BIT21
#define KEY_MENU_STOP				KEY_BIT20

#define KEY_MENU_COUNTER			KEY_BIT24 | KEY_BIT2
#define KEY_MENU_MOLD            KEY_BIT23 | KEY_BIT2
#define KEY_MENU_IO              KEY_BIT22 | KEY_BIT2
#define KEY_MENU_CYCLE				KEY_BIT21 | KEY_BIT2
#define KEY_MENU_MANUAL				KEY_BIT20 | KEY_BIT2

#define KEY_ARROW_UP					KEY_BIT19
#define KEY_ARROW_DOWM				KEY_BIT18
#define KEY_ARROW_LEFT				KEY_BIT17
#define KEY_ARROW_RIGHT				KEY_BIT16
#define KEY_REJECT					KEY_BIT15

#define KEY_ERROR_LOG				KEY_BIT19 | KEY_BIT2
#define KEY_DISP_VERSION			KEY_BIT18 | KEY_BIT2
#define KEY_LANGUAGE					KEY_BIT16 | KEY_BIT2
#define KEY_DETECT_EN				KEY_BIT13 | KEY_BIT2	
	
#define KEY_ALARM						KEY_BIT14
#define KEY_DETECT					KEY_BIT13
#define KEY_EJECTOR					KEY_BIT12
#define KEY_MAIN_ROTATE				KEY_BIT11
#define KEY_RUNNER_UPDOWN			KEY_BIT10
#define KEY_NO_7						KEY_BIT14
#define KEY_NO_8                 KEY_BIT13
#define KEY_NO_9                 KEY_BIT12
#define KEY_NO_6						KEY_BIT11
	
#define KEY_MAIN_UPDOWN				KEY_BIT9
#define KEY_MAIN_FW					KEY_BIT8
#define KEY_MAIN_VACUUM				KEY_BIT7
#define KEY_MAIN_CHUCK_ROTATE		KEY_BIT6
#define KEY_RUNNER_GRIPPER			KEY_BIT5	
	
#define KEY_NO_4						KEY_BIT9
#define KEY_NO_5                 KEY_BIT8
#define KEY_NO_2                 KEY_BIT7
#define KEY_NO_3				      KEY_BIT6

#define KEY_MAIN_CHUCK				KEY_BIT4
#define KEY_OP							KEY_BIT3
#define KEY_SHIFT						KEY_BIT2
#define KEY_CLEAR						KEY_BIT1
#define KEY_ENTER						KEY_BIT0
	
#define KEY_NO_1						KEY_BIT4
#define KEY_NO_0				      KEY_BIT3
	
#define KEY_NONE						0x00000000
	
typedef uint32_t KeyTypeDef;	


//extern KeyTypeDef Key;					// 7 6 5 4  3 2 1 0   
								


void LoopAppKey(void);
uint32_t GetKey(void);

#ifdef	__cplusplus
}
#endif

#endif	/* __APPTKEY_H */