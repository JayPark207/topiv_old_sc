/* 
 * File:   __APPTLCD_H
 * Author: Jay Park
 *
 * Created on 2016�� 11�� 14�� (Mon), PM 3:33
 */

#ifndef __APPLCD_H
#define	__APPLCD_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "interface.h"
#include "main.h"   
   
/****************************************************/
/*		define of LCD access value					*/
/****************************************************/
#define LCD_CS_BIT_MASK				0xFFFFFFFB
#define LCD_BASE_ADDR				0x100000
#define LCD_RESET_PORT				rPDAT7
#define LCD_RESET_MASK				0xBF
#define LCD_RESET_EN				0x40
#define LCD_BACKLIGHT_PORT			rPDAT5
#define LCD_BACKLIGHT_MASK			0xEF
#define LCD_BACKLIGHT_EN			0x10
#define LCD_CS1						0x00
#define LCD_CS2						0x04
#define LCD_DISPLAY_ON_OFF_ADDR		(LCD_BASE_ADDR + 0x00)
#define LCD_DISPLAY_ON_OFF_DATA		0x3E
#define LCD_DISPLAY_START_ADDR		(LCD_BASE_ADDR + 0x00)
#define LCD_DISPLAY_START_DATA		0xC0
#define LCD_PAGE_ADDR_SET_ADDR		(LCD_BASE_ADDR + 0x00)
#define LCD_PAGE_ADDR_SET_DATA		0xB8
#define LCD_SET_ADDRESS_ADDR		(LCD_BASE_ADDR + 0x00)
#define LCD_SET_ADDRESS_DATA		0x40
#define LCD_STATUS_READ_ADDR		(LCD_BASE_ADDR + 0x01)
#define LCD_WRITE_DISPLAY_DATA_ADDR	(LCD_BASE_ADDR + 0x02)
#define LCD_READ_DISPLAY_DATA_ADDR	(LCD_BASE_ADDR + 0x03)
	
#define GLCD_RS_INSTRUCTION		0x00
#define GLCD_RS_DATA				0x01
	
#define GLCD_CS1		0x01	//LEFT
#define GLCD_CS2		0x02	//RIGHT
#define GLCD_CS_BOTH	0x03	//BOTH
	
#define GLCD_DISPLAY_ON		0x3F
#define GLCD_DISPLAY_OFF	0x3E
	
#define GLCD_DATA_GPIO_Port GPIOA
	
#define GLCD_CMD_SET_Y_ADDRESS			0x40
#define GLCD_CMD_SET_PAGE					0xB8
#define GLCD_CMD_DISPLAY_START_LINE		0xC0

#define TIME_LCD_TEST			1000
#define TIME_us_LCD				20

#define GLCD_RS_HIGH()              HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_SET)
#define GLCD_RS_LOW()               HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET)

#define GLCD_Read()                 HAL_GPIO_WritePin(LCD_RW_GPIO_Port, LCD_RW_Pin, GPIO_PIN_SET)
#define GLCD_Write()                HAL_GPIO_WritePin(LCD_RW_GPIO_Port, LCD_RW_Pin, GPIO_PIN_RESET)

#define GLCD_ENABLE_HIGH()          HAL_GPIO_WritePin(LCD_EN_GPIO_Port, LCD_EN_Pin, GPIO_PIN_SET)
#define GLCD_ENABLE_LOW()           HAL_GPIO_WritePin(LCD_EN_GPIO_Port, LCD_EN_Pin, GPIO_PIN_RESET)

#define GLCD_CS1_HIGH()             HAL_GPIO_WritePin(LCD_CS0_GPIO_Port, LCD_CS0_Pin, GPIO_PIN_SET)
#define GLCD_CS1_LOW()              HAL_GPIO_WritePin(LCD_CS0_GPIO_Port, LCD_CS0_Pin, GPIO_PIN_RESET)
 
#define GLCD_CS2_HIGH()             HAL_GPIO_WritePin(LCD_CS1_GPIO_Port, LCD_CS1_Pin, GPIO_PIN_SET)
#define GLCD_CS2_LOW()              HAL_GPIO_WritePin(LCD_CS1_GPIO_Port, LCD_CS1_Pin, GPIO_PIN_RESET)
	
#define STATUS_BUSY_CHECK        	HAL_GPIO_ReadPin(LCD_D7_GPIO_Port, LCD_D7_Pin)
	/* Defines -------------------------------------------------------------------*/

#define  GLCD_Display_ON           	GLCD_DISPLAY_ON
#define  GLCD_Display_OFF           GLCD_DISPLAY_OFF
#define  START_LINE                 GLCD_CMD_DISPLAY_START_LINE 
#define  PAGE                       GLCD_CMD_SET_PAGE
#define  COLUMN                     GLCD_CMD_SET_Y_ADDRESS
	
	void LoopAppLCD(void);
#ifdef	__cplusplus
}
#endif

#endif	/* __APPLCD_H */