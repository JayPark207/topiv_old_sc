#include "timer.h"

static volatile uint64_t uwTick;
uint64_t HAL_GetTick(void);

void SetSysTick(uint64_t *pExpiredTime, uint64_t time)
{
    uint64_t currentSystick;
   
    currentSystick = HAL_GetTick();
    *pExpiredTime = currentSystick + time;
}
uint8_t ChkExpireSysTick(uint64_t* pExpiredTime)
{
    uint64_t tick;
    tick = HAL_GetTick();
        if(tick >= *pExpiredTime)   // expired
            return -1;
        else
            return 0;               // it hasn't been expired yet
}
void SetStopWatch(uint64_t* pPastTime)
{
	*pPastTime = HAL_GetTick();
}
uint64_t RemainStopWatch(uint64_t* pPastTime, uint64_t Time)
{
	uint64_t NowTime;
	NowTime = HAL_GetTick();
	if((*pPastTime + Time) < NowTime) return 0;
	
	return ((*pPastTime + Time) - NowTime);
}
uint64_t NowWatch(uint64_t* pPastTime)
{
	uint64_t NowTime;
	NowTime = HAL_GetTick();
	return (NowTime - *pPastTime);
}
void HAL_IncTick(void)
{
	uwTick++;
}

uint64_t HAL_GetTick(void)
{
  return uwTick;
}