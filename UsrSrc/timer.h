/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TIMER_H
#define __TIMER_H
#ifdef __cplusplus
 extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

void SetSysTick(uint64_t *pExpiredTime, uint64_t time);
uint8_t ChkExpireSysTick(uint64_t* pExpiredTime);

void SetStopWatch(uint64_t* pPastTime);
uint64_t RemainStopWatch(uint64_t* pPastTime, uint64_t Time);

#ifdef __cplusplus
}
#endif
#endif /*__ __TIMER_H */